package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/frozentech/api"
	"github.com/frozentech/array"
	"github.com/frozentech/logger"
	"github.com/frozentech/logs"
	"gitlab.com/frozentech/comment/database"
	"gitlab.com/frozentech/comment/resource"
	"go.uber.org/zap"
)

// init ...
func init() {
	var err error

	database.Connection, err = database.Connect()
	if err != nil {
		panic(err)
	}

}

func main() {
	lambda.Start(func(ctx context.Context, req events.APIGatewayProxyRequest) (resp events.APIGatewayProxyResponse, err error) {

		xray.Configure(xray.Config{})

		lc, _ := lambdacontext.FromContext(ctx)

		logger.Client = logger.New().With(
			zap.String("appname", "astrea"),
			zap.String("traceID", req.Headers["X-Amzn-Trace-Id"]),
			zap.String("requestID", lc.AwsRequestID),
			zap.String("endpoint", fmt.Sprintf("%s %s", req.HTTPMethod, req.Path)))

		logs.Story = logs.New()

		defer func() {
			logger.Client.Info("API",
				zap.Any(
					"log",
					array.DeleteEmpty(
						strings.Split(strings.Replace(logs.Story.Dump(false), "\t", "   ", -1), "\n"),
					),
				),
			)
			logger.Client.Sync()

		}()

		resp, err = api.NewHandler(resource.NewHandler())(ctx, req)

		// Set header to allow CORS
		if cors, ok := req.RequestContext.Authorizer["cors"].(string); ok && cors != "" {
			resp.Headers["Access-Control-Allow-Origin"] = cors
		}
		return
	})
}
